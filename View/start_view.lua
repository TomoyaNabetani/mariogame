
module( ...,package.seeall)



-- Background
local background = display.newImage( ImgDir .. 'background.png', backGroup)
background.x = centerX
background.y = centerY


local status = {}

-- Display lives, score
local lives = 3
local score = 0

local livesText = display.newText( "Lives: " .. lives, 100, 25, native.systemFont, 25 )
local scoreText = display.newText( "Score: " .. score, 220, 25, native.systemFont, 25 )

function getData4()
    return status
end



-- Tree1
local trees1 = {}
for i=1,10 do
    trees1[i] = display.newImage( ImgDir .. "tree1.png" )
    trees1[i].x = (centerX + 300) * i
    trees1[i].y = display.contentHeight - 220
    trees1[i].xScale = 0.2
    trees1[i].yScale = 0.2
    trees1[i].alpha = 0.5
end

for index,tree1 in pairs( trees1 ) do
    physics.addBody( tree1, "kinematic",{ isSensor=true } )
    tree1:setLinearVelocity( -50, 0 )
end


-- Tree2
local trees2 = {}
for i=1,10 do
    trees2[i] = display.newImage( ImgDir .. "tree2.png" )
    trees2[i].x = (centerX + 400) * i
    trees2[i].y = display.contentHeight - 150
    trees2[i].xScale = 0.2
    trees2[i].yScale = 0.2
    trees2[i].alpha = 0.5
end

for index,tree2 in pairs( trees2 ) do
    physics.addBody( tree2, "kinematic", { isSensor=true } )
    tree2:setLinearVelocity( -60, 0 )
end


-- Tree3
local trees3 = {}
for i=1,10 do
    trees3[i] = display.newImage( ImgDir .. "tree3.png" )
    trees3[i].x = (centerX + 500) * i
    trees3[i].y = display.contentHeight - 80
    trees3[i].xScale = 0.1
    trees3[i].yScale = 0.1
    trees3[i].alpha = 0.5
end

for index,tree3 in pairs( trees3 ) do
    physics.addBody( tree3, "kinematic", { isSensor=true } )
    tree3:setLinearVelocity( -50, 0 )
end

local heroAnimation = {}

-- Hero
local heroImageSheet = graphics.newImageSheet( ImgDir .. "pengwin.png", { width=100, height=105, numFrames=6 } )
local heroAnimation = display.newSprite( heroImageSheet, { name="hero", start=1, count=6, time=800 } )
heroAnimation.x = display.contentWidth * 0.2    
heroAnimation.y = 250
heroAnimation.xScale = 0.5     
heroAnimation.yScale = 0.6
heroAnimation.myName = "hero"
local heromodel = { -30, 40, 40, 40, 40, -20, -30, -20 }
physics.addBody( heroAnimation, "dynamic", { denisity=1, friction=0, bounce=0, shape=heromodel } ) 


function getData1()
    return heroAnimation
end


-- Enemy
local enemies = {}
for i=1,10 do
enemies[i] = display.newImage(ImgDir ..  "enemy.png",mainGroup, 10, 10 )
enemies[i].x = (centerX + 100) * i
enemies[i].y = display.contentHeight * 0.78
enemies[i].xScale = 0.22
enemies[i].yScale = 0.22
enemies[i].myName = "enemy"
end

local enemymodel = { -15, 23, 10, 23, 10, -15, -15, -15 }

for index,enemy in pairs( enemies ) do
    physics.addBody( enemy, "dynamic", { denisity=1, friction=0, bounce=0, shape=enemymodel } )
    enemy:setLinearVelocity(-70,0)
end

-- Coin
local coins = {}
for i=1,10 do
    coins[i] = display.newImage( ImgDir ..  "coin.png" )
    coins[i].x = (centerX + 350) * i
    coins[i].y = display.contentHeight * 0.5
    coins[i].xScale = 0.15
    coins[i].yScale = 0.15
    coins[i].myName = "coin"
end

local coinmodel = { -10, 10, 10, 10, 10, -10, -10, -10 }
for index,coin in pairs( coins ) do
    physics.addBody( coin, "kinematic", { denisity=0, friction=0, bounce=0, shape=coinmodel, isSensor=true } )
    coin:setLinearVelocity( -70, 0 )
end

-- Fish
local fishes = {}
for i=1,10 do
    fishes[i] = display.newImage( ImgDir .. "fish.png" )
    fishes[i].x = (centerX + 600) * i
    fishes[i].y = display.contentHeight * 0.5
    fishes[i].xScale = 0.15
    fishes[i].yScale = 0.15
    fishes[i].myName = "fish"
end

local fishmodel = { -10, 10, 10, 10, 10, -10, -10, -10 }
for index,fish in pairs( fishes ) do
    physics.addBody( fish, "kinematic", { denisity=0, friction=0, bounce=0, shape=fishmodel, isSensor=true } )
    fish:setLinearVelocity( -70, 0 )

end


local  buttonA = {}
-- ButtonA
local buttonA = display.newImage( ImgDir .. "buttonA.png", 10, 10 )
buttonA.x = 350
buttonA.y = 100
buttonA.xScale = 0.5
buttonA.yScale = 0.5
buttonA.alpha = 0.5

function getData2()
    return buttonA
end



local buttonB = {}
-- ButtonB
local buttonB = display.newImage( ImgDir .. "buttonB.png", 10, 10 )
buttonB.x = 420
buttonB.y = 100
buttonB.xScale = 0.5
buttonB.yScale = 0.5
buttonB.alpha = 0.5

function getData3()
    return buttonB
end

-- Ground
local blueGround = display.newRect( 0, 0, 2000, 40 )
blueGround:setFillColor( 0, 255/255, 255/255)
blueGround.x = centerX
blueGround.y = 315
blueGround.myName = "ground"
physics.addBody( blueGround, "static", {} ) 












