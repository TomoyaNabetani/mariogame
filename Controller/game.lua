
-- Physics
local physics = require( "physics" )
physics.start()
local lives = 3
local score = 0


local heroAnimation = require( "View.start_view" )
local heroAnimation = heroAnimation.getData1()

local buttonA = require( "View.start_view" )
local buttonA = buttonA.getData2()

local buttonB = require( "View.start_view")
local buttonB = buttonB.getData3()

local status = require( "View.start_view" )
local status = status.getData4()


local model = require( "model.game_model")
local model = model.getData7()









-- Set up display groups
local backGroup = display.newGroup()  
local mainGroup = display.newGroup()  
local uiGroup = display.newGroup()    


local collisionOnGround = true
heroAnimation:play()

-- Fire
local function fireLaser()

    local newLaser = display.newImageRect(mainGroup,ImgDir .. "fire.png", 40, 40 )
    physics.addBody( newLaser, "static", { isSensor=true } )
    newLaser.isBullet = true
    newLaser.myName = "laser"
    newLaser:toBack()

    newLaser.x = heroAnimation.x
    newLaser.y = heroAnimation.y
    transition.to( newLaser, { x=500, time=1000,
        onComplete = function() display.remove( newLaser ) end
     } )
end


-- Jump
local function jump()
     if collisionOnGround == true then  
        collisionOnGround = false
        
        heroAnimation:pause()
        heroAnimation:applyLinearImpulse( 0, -0.5, heroAnimation.x, heroAnimation.y )
        heroAnimation.isFixedRotation = true 
    end
end




-- Collision
local function onCollision(event)
    local obj1 = event.object1
    local obj2 = event.object2

    if( ( obj1.myName == "ground" and obj2.myName == "hero" ) or
        ( obj1.myName == "hero" and obj2.myName == "ground" ) )
    then
        if event.phase == "began" and collisionOnGround == false then
            collisionOnGround = true
            heroAnimation:play()
        end
    end
    

    if( ( obj1.myName == "laser" and obj2.myName == "enemy" ) or
        ( obj1.myName == "enemy" and obj2.myName == "laser" ) )
    then
        display.remove( obj1 )
        display.remove( obj2 )

        local score1 = {}
        score = score + 1000
        function getData8()
            return score1
        end

        local model = require( "Model.game_model")
        local model = model.getData7() 
        
    end

    if( ( obj1.myName == "hero" and obj2.myName == "enemy" ) or
        ( obj1.myName == "enemy" and obj2.myName == "hero" ) )
    then
        display.remove( obj2 )
        lives = lives - 1
        local model = require( "Model.game_model")
        local model = model.getData7() 
        

        if lives == 0 then
            local clear = require( "View.clear_view")
            local clear = clear.getData6()
        end
    end
    if( ( obj1.myName == "hero" and obj2.myName == "fish" ) or
        ( obj1.myName == "fish" and obj2.myName == "hero" ) )
    then
        display.remove(obj2)
        lives = lives + 1
        local model = require( "Model.game_model")
        local model = model.getData7() 
        
    end
    if( ( obj1.myName == "hero" and obj2.myName == "coin" ) or
        ( obj1.myName == "coin" and obj2.myName == "hero" ) )
    then
        display.remove(obj2)
        score = score + 50 
        local model = require( "Model.game_model")
        local model = model.getData7() 
        
    end
end




buttonA:addEventListener( "tap", jump )
buttonB:addEventListener( "tap", fireLaser )
Runtime:addEventListener( "collision", onCollision )



-- Result
local  function resultDisplay( event )
    if lives > 0 then
        local clear = require( "View.clear_view")
        local clear = clear.getData5()   
    end
end 


local DELAYTIME = 100*1000
timer.performWithDelay( DELAYTIME, resultDisplay) 


