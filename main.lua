-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------
-- Physics
local physics = require( "physics")
physics.start()


local centerX = display.contentCenterX
local centerY = display.contentCenterY


-- Initialize variables
local lives = 3
local score = 0
local livesText
local scoreText

-- Set up display groups
local backGroup = display.newGroup()  
local mainGroup = display.newGroup()  
local uiGroup = display.newGroup()    


-- Background
local background = display.newImageRect( backGroup, "background.png", 620, 420 )
background.x = display.contentCenterX
background.y = display.contentCenterY


-- Display lives, score
livesText = display.newText( uiGroup, "Lives: " .. lives, 100, 25, native.systemFont, 25 )
scoreText = display.newText( uiGroup, "Score: " .. score, 220, 25, native.systemFont, 25 )


-- Hide statusbar
display.setStatusBar( display.HiddenStatusBar )


-- Clear
local function  clear()
    local backred = display.newRect( centerX, centerY, display.contentWidth, display.contentHeight  )
    backred:setFillColor( 0, 0, 255 )
    display.newText( "Clear", centerX, centerY, nil, 40 )
    display.newText( "Score:"..score, centerX, centerY+100, nil, 20 )
end

-- Failed
local function failed()
    local backblack = display.newRect( centerX, centerY, display.contentWidth, display.contentHeight  )
    backblack:setFillColor( 0, 0, 0 )
    display.newText( "〜失敗〜", centerX, centerY, nil, 40 )
    display.newText( "Score:"..score, centerX, centerY+100, nil, 20 )
end


-- Tree1
local trees1 = {}
for i=1,10 do
    trees1[i] = display.newImage( backGroup,"tree1.png" )
    trees1[i].x = (centerX + 300) * i
    trees1[i].y = display.contentHeight - 220
    trees1[i].xScale = 0.2
    trees1[i].yScale = 0.2
    trees1[i].alpha = 0.5
end

for index,tree1 in pairs( trees1 ) do
    physics.addBody( tree1, "kinematic",{ isSensor=true } )
    tree1:setLinearVelocity( -50, 0 )
end


-- Tree2
local trees2 = {}
for i=1,10 do
    trees2[i] = display.newImage( backGroup,"tree2.png" )
    trees2[i].x = (centerX + 400) * i
    trees2[i].y = display.contentHeight - 150
    trees2[i].xScale = 0.2
    trees2[i].yScale = 0.2
    trees2[i].alpha = 0.5
end

for index,tree2 in pairs( trees2 ) do
    physics.addBody( tree2, "kinematic", { isSensor=true } )
    tree2:setLinearVelocity( -60, 0 )
end


-- Tree3
local trees3 = {}
for i=1,10 do
    trees3[i] = display.newImage( backGroup,"tree3.png" )
    trees3[i].x = (centerX + 500) * i
    trees3[i].y = display.contentHeight - 80
    trees3[i].xScale = 0.1
    trees3[i].yScale = 0.1
    trees3[i].alpha = 0.5
end

for index,tree3 in pairs( trees3 ) do
    physics.addBody( tree3, "kinematic", { isSensor=true } )
    tree3:setLinearVelocity( -50, 0 )
end


local collisionOnGround = true


-- Hero
local heroImageSheet = graphics.newImageSheet( "pengwin.png", { width=100, height=105, numFrames=6 } )
local heroAnimation = display.newSprite( heroImageSheet, { name="hero", start=1, count=6, time=800 } )
heroAnimation.x = display.contentWidth * 0.2    
heroAnimation.y = 250
heroAnimation.xScale = 0.5     
heroAnimation.yScale = 0.6
heroAnimation.myName = "hero"
local heromodel = { -30, 40, 40, 40, 40, -20, -30, -20 }
physics.addBody( heroAnimation, "dynamic", { denisity=1, friction=0, bounce=0, shape=heromodel } ) 
heroAnimation:play()    


-- Enemy
local enemies = {}
for i=1,10 do
enemies[i] = display.newImage(mainGroup, "enemy.png", 10, 10 )
enemies[i].x = (centerX + 100) * i
enemies[i].y = display.contentHeight * 0.78
enemies[i].xScale = 0.22
enemies[i].yScale = 0.22
enemies[i].myName = "enemy"
end

local enemymodel = { -15, 23, 10, 23, 10, -15, -15, -15 }

for index,enemy in pairs( enemies ) do
    physics.addBody( enemy, "dynamic", { denisity=1, friction=0, bounce=0, shape=enemymodel } )
    enemy:setLinearVelocity(-70,0)
end


-- Ground
local function onGround(event)
    if event.phase == "began" and collisionOnGround == false then
        collisionOnGround = true
        heroAnimation:play()
    end
end

local blueGround = display.newRect( 0, 0, 2000, 40 )
blueGround:setFillColor( 0, 255/255, 255/255)
blueGround.x = centerX
blueGround.y = 315
physics.addBody( blueGround, "static", {} ) 
blueGround:addEventListener("collision", onGround)


-- Fire
local function fireLaser()

	local newLaser = display.newImageRect(mainGroup,"fire.png", 40, 40 )
	physics.addBody( newLaser, "static", { isSensor=true } )
	newLaser.isBullet = true
	newLaser.myName = "laser"

	newLaser.x = heroAnimation.x
	newLaser.y = heroAnimation.y
	newLaser:toBack()
	transition.to( newLaser, { x=500, time=1000,
		onComplete = function() display.remove( newLaser ) end
	 } )
end


-- Coin
local coins = {}
for i=1,10 do
    coins[i] = display.newImage( "coin.png" )
    coins[i].x = (centerX + 350) * i
    coins[i].y = display.contentHeight * 0.5
    coins[i].xScale = 0.15
    coins[i].yScale = 0.15
    coins[i].myName = "coin"
end

local coinmodel = { -10, 10, 10, 10, 10, -10, -10, -10 }
for index,coin in pairs( coins ) do
    physics.addBody( coin, "kinematic", { denisity=0, friction=0, bounce=0, shape=coinmodel, isSensor=true } )
    coin:setLinearVelocity( -70, 0 )
end


-- Fish
local fishes = {}
for i=1,10 do
    fishes[i] = display.newImage( "fish.png" )
    fishes[i].x = (centerX + 600) * i
    fishes[i].y = display.contentHeight * 0.5
    fishes[i].xScale = 0.15
    fishes[i].yScale = 0.15
    fishes[i].myName = "fish"
end

local fishmodel = { -10, 10, 10, 10, 10, -10, -10, -10 }
for index,fish in pairs( fishes ) do
    physics.addBody( fish, "kinematic", { denisity=0, friction=0, bounce=0, shape=fishmodel, isSensor=true } )
    fish:setLinearVelocity( -70, 0 )

end


-- Jump
local function jump()
     if collisionOnGround == true then  
        collisionOnGround = false
        heroAnimation:pause()
        heroAnimation:applyLinearImpulse( 0, -0.5, heroAnimation.x, heroAnimation.y )
        heroAnimation.isFixedRotation = true 
    end
end


-- ButtonA
buttonA = display.newImage( "buttonA.png", 10, 10 )
buttonA.x = 350
buttonA.y = 100
buttonA.xScale = 0.5
buttonA.yScale = 0.5
buttonA.alpha = 0.5

-- ButtonB
buttonB = display.newImage( "buttonB.png", 10, 10 )
buttonB.x = 420
buttonB.y = 100
buttonB.xScale = 0.5
buttonB.yScale = 0.5
buttonB.alpha = 0.5


buttonA:addEventListener("tap", jump )
buttonB:addEventListener("tap", fireLaser )


-- Collision
local function onCollision(event)
    local obj1 = event.object1
    local obj2 = event.object2
    

    if( ( obj1.myName == "laser" and obj2.myName == "enemy" ) or
        ( obj1.myName == "enemy" and obj2.myName == "laser" ) )
    then
        display.remove( obj1 )
        display.remove( obj2 )
        score = score + 1000
        scoreText.text = "Score:"..score 
    end

    if( ( obj1.myName == "hero" and obj2.myName == "enemy" ) or
        ( obj1.myName == "enemy" and obj2.myName == "hero" ) )
    then
        display.remove( obj2 )
        lives = lives - 1
        livesText.text = "Lives:"..lives

        if lives == 0 then
            failed()
        end
    end
    if( ( obj1.myName == "hero" and obj2.myName == "fish" ) or
        ( obj1.myName == "fish" and obj2.myName == "hero" ) )
    then
        display.remove(obj2)
        lives = lives + 1
        livesText.text = "Lives:"..lives 
    end
    if( ( obj1.myName == "hero" and obj2.myName == "coin" ) or
        ( obj1.myName == "coin" and obj2.myName == "hero" ) )
    then
        display.remove(obj2)
        score = score + 50 
        scoreText.text = "Score:"..score 
    end
end

Runtime:addEventListener( "collision", onCollision )


-- Result
local  function resultDisplay( event )
    if lives > 0 then
        clear()
    end

end 

local DELAYTIME = 40*1000
timer.performWithDelay( DELAYTIME, resultDisplay) 


